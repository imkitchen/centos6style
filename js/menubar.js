// menubar.js
// yosuke tamura

$(document).ready(function() {
	$(".navbar").find('li').each(function(index) {
		if ($(this).find('ul').length > 0) {

		    //ホバー時にサブメニューを表示する
		    $(this).mouseenter(function() {
			    $(this).find('ul').show();
		    });
 
		    //マウスが離れた時にサブメニューを隠す
		    $(this).mouseleave(function() {
			    $(this).find('ul').hide();  
		    });
		}
	});
 
});
